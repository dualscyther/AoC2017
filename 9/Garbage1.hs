import Control.Monad.State

{-
Does not account for invalid input.
The state tuple is (tier, inGarbage, score)
-}
countScore :: (Integral a) => [Char] -> State (a, Bool, a) a
countScore [] = do
    (_, _, score) <- get
    return score
countScore ('!':_:xs) = countScore xs
countScore (x:xs) = do
    (tier, inGarbage, score) <- get
    let
        newState = case inGarbage of
            True -> case x of
                '>' -> (tier, False, score)
                _ -> (tier, True, score)
            False -> case x of
                '{' -> (tier + 1, False, score)
                '}' -> (tier - 1, False, score + tier)
                '<' -> (tier, True, score)
                _ -> (tier, False, score)
        in
            put newState
    countScore xs

main :: IO ()
main = do
    line <- getLine
    print $ evalState (countScore line) (0, False, 0)
    -- print $ runState (countScore line) $ (0, False, 0)