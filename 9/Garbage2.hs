import Control.Monad.State

{-
Does not account for invalid input.
The state tuple is (tier, inGarbage, score, totalGarbage)
-}
countGarbage :: (Integral a) => [Char] -> State (a, Bool, a, a) a
countGarbage [] = do
    (_, _, _, garbage) <- get
    return garbage
countGarbage ('!':_:xs) = countGarbage xs
countGarbage (x:xs) = do
    (tier, inGarbage, score, garbage) <- get
    let
        newState = case inGarbage of
            True -> case x of
                '>' -> (tier, False, score, garbage)
                _ -> (tier, True, score, garbage + 1)
            False -> case x of
                '{' -> (tier + 1, False, score, garbage)
                '}' -> (tier - 1, False, score + tier, garbage)
                '<' -> (tier, True, score, garbage)
                _ -> (tier, False, score, garbage)
        in
            put newState
    countGarbage xs

main :: IO ()
main = do
    line <- getLine
    print $ evalState (countGarbage line) (0, False, 0, 0)