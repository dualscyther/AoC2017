const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  // Don't echo
  terminal: false,
});

// nums: number[]
const nums = Array(256).fill(0).map((_, i) => i);
// lengths: number[] | void
let lengths;
let pos = 0, skip = 0;

rl.on("line", line => {
    if (lengths == undefined) {
        lengths = [...line]
            .map(s => s.charCodeAt(0))
            .concat([17,31,73,47,23]);
        console.log(`lengths = ${lengths}`);
    }
});
rl.on("close", handleClose);

function hashRound() {
    lengths.forEach(l => {
        for (let i = 0; i < (l - 1)/2; i++) {
            const firstPos = (pos + i) % 256;
            const secondPos = (pos + l - i - 1) % 256;
            const tmp = nums[firstPos];
            nums[firstPos] = nums[secondPos];
            nums[secondPos] = tmp;
        }

        pos = (pos + l + skip) % 256;
        skip++;
    })
}

function handleClose() {
    for (let i = 0; i < 64; i++) {
        hashRound();
    }
    console.log(nums)

    const denseHash = Array(16).fill(0).map((_, i) => {
        const slice = nums.slice(i * 16, (i + 1) * 16);
        return slice.reduce((acc, x) => acc ^ x);
    });
    const hex = denseHash.reduce(
        (acc, x) => acc + x.toString(16).padStart(2, '0'),
        ""
    )

    console.log(`final list = ${hex}`);
}
