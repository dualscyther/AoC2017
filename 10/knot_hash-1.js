const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  // Don't echo
  terminal: false,
});

// nums: number[]
const nums = Array(256).fill(0).map((_, i) => i);
// lengths: number[] | void
let lengths;

rl.on("line", line => {
    if (lengths == undefined) {
        console.log(line.split(","));
        lengths = line.split(",").map(x => Number.parseInt(x));
        console.log(`lengths = ${lengths}`);
    }
});
rl.on("close", handleClose);

function handleClose() {
    // compute hash
    let pos = 0, skip = 0;
    lengths.forEach(l => {
        for (i = 0; i < (l - 1)/2; i++) {
            const firstPos = (pos + i) % 256
            const secondPos = (pos + l - i - 1) % 256
            const tmp = nums[firstPos];
            nums[firstPos] = nums[secondPos];
            nums[secondPos] = tmp;
        }

        pos = (pos + l + skip) % 256
        skip++;
    })


    console.log(`final list = ${nums}`);
    console.log(`result = ${nums[0] * nums[1]}`);
}
