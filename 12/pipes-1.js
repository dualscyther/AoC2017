const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  // Don't echo
  terminal: false,
});

// adjacency list
// graph: (number[] | void)[]
const graph = [];

rl.on("line", line => {
    graph.push(line.split("<->")[1].split(",").map(x => Number.parseInt(x)));
});
rl.on("close", handleClose);

function handleClose() {
    // we will run a DFS starting at vertex 0
    // DFS because I think the browser engine most likely either implements
    // arrays using hashtables or C-arrays, so using a stack will have
    // the best performance characteristics
    const seen = new Set();
    const toSee = [0];
    while (toSee.length > 0) {
        u = toSee.pop();
        if (seen.has(u)) {
            continue;
        }
        seen.add(u);
        toSee.push.apply(toSee, graph[u]);
    }
    console.log(seen.size)
}
