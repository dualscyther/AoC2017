const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  // Don't echo
  terminal: false,
});

// adjacency list
// graph: (number[] | void)[]
const graph = [];
let n = 0;

rl.on("line", line => {
    graph.push(line.split("<->")[1].split(",").map(x => Number.parseInt(x)));
    n++;
});
rl.on("close", handleClose);

function handleClose() {
    console.log(`There are ${n} programs`);
    const seen = new Set();
    let totalSets = 0;

    for (let i = 0; i < n; i++) {
        if (seen.has(i)) {
            continue;
        }

        // if we haven't seen this vertex then start a new DFS
        totalSets++;
        const toSee = [i];
        while (toSee.length > 0) {
            u = toSee.pop();
            if (seen.has(u)) {
                continue;
            }
            seen.add(u);
            toSee.push.apply(toSee, graph[u]);
        }
    }

    console.log(totalSets);
}
