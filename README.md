# Dano's solutions to Advent of Code 2017

<http://adventofcode.com/2017/>

You might notice the inconsistencies in naming conventions, code quality, or
something else. This is because I've taken my sweet time in doing these coding
challenges.

## Running times

1.  Inverse Captcha
    1.  O(n)
    1.  O(n)
1.  Corruption Checksum
    1.  O(n)
    1.  O(n<sup>2</sup>)
1.  Spiral memory
    1.  O(1)
    1.  O(n)
1.  High-Entropy Passphrases
    1.  O(nlogn)
    1.  O(nlogn)
1.  A Maze of Twisty Trampolines, All Alike
    1.  O(n<sup>2</sup>)
    1.  Not sure @todo
1.  Memory Reallocation
    1.  Very long
    1.  Very long (same as Part 1)
1.  Recursive Circus
    1.  O(n + m) average if object key lookup is O(1) average
    1.  Keepo
