-- import qualified Data.Map as Map
import Data.List

data Instruction a = Instruction
    { registerName :: String
    , op :: a -> a -> a
    , opValue :: a
    , conditionRegisterName :: String
    , conditionOp :: a -> a -> Bool
    , conditionValue :: a
    }

type Register a = (String, a)

checkRegister :: (Num a) => String -> (a -> a -> Bool) -> a -> [Register a] -> Bool
checkRegister name f value ys = (snd register) `f` value
  where
    register = case find ((== name) . fst) ys of
        Just a -> a
        Nothing -> (name, 0)

executeLine :: (Num a) => [Register a] -> Instruction a -> [Register a]
executeLine acc x = before ++ [newRegister] ++ after
  where
    register = case find ((== registerName x) . fst) acc of
        Just a -> a
        Nothing -> (registerName x, 0)
    before = takeWhile ((/= registerName x) . fst) acc
    after = case dropWhile ((/= registerName x) . fst) acc of
        [] -> []
        (x:xs) -> xs
    newRegister =
        if checkRegister (conditionRegisterName x) (conditionOp x) (conditionValue x) acc
        then (fst register, op x (snd register) (opValue x))
        else register

parseLine :: (Num a, Ord a, Read a) => String -> Instruction a
parseLine xs =
    (Instruction
        (tokens !! 0)
        (case (tokens !! 1) of
            "inc" -> (+)
            "dec" -> (-))
        (read (tokens !! 2))
        (tokens !! 4)
        (case (tokens !! 5) of
            "<" -> (<)
            "<=" -> (<=)
            ">" -> (>)
            ">=" -> (>=)
            "==" -> (==)
            "!=" -> (/=))
        (read (tokens !! 6))
    )
  where
    tokens = words xs

main = do
    contents <- fmap lines getContents
    let instructions = fmap parseLine contents
    let finalState = foldl executeLine [] instructions
    print $ foldl max 0 (fmap snd finalState)
