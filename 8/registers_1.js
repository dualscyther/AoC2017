const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  // Don't echo
  terminal: false,
});

const registers = new Map();
const ops = {
  "inc": (a, b) => a + b,
  "dec": (a, b) => a - b,
};
const comparisonOps = {
  ">": (a, b) => a > b,
  ">=": (a, b) => a >= b,
  "<": (a, b) => a < b,
  "<=": (a, b) => a <= b,
  "==": (a, b) => a == b,
  "!=": (a, b) => a != b,
};

rl.on("line", line => {
  const [
    registerName,
    opString,
    opValue,
    ,
    compareRegisterName,
    compareOpString,
    compareValue,
  ] = line.split(" ");

  const compareRegisterValue = registers.has(compareRegisterName)
    ? registers.get(compareRegisterName)
    : 0;
  if (!comparisonOps[compareOpString](compareRegisterValue, parseInt(compareValue, 10))) {
    return;
  }

  const oldRegisterValue = registers.has(registerName) ? registers.get(registerName) : 0;
  registers.set(registerName, ops[opString](oldRegisterValue, parseInt(opValue, 10)));
});

rl.on("close", () => {
  console.log(Math.max(...registers.values()));
});