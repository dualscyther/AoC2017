import Data.List.Split

data Coord x y = Coord {getX :: x, getY :: y} deriving Show

main = do
    line <- getLine
    -- we don't care whether coordinates are negative
    let (Coord x y) = nextPos (Coord 0 0) (splitOn "," line)
        steps = (abs x) + abs ((abs y) -  0.5 * (abs x))
        in putStrLn (show (steps))

nextPos :: (Num x) => (Fractional y) => Coord x y -> [String] -> Coord x y
nextPos coord [] = coord
nextPos (Coord x y) (dir:dirs) = nextPos (Coord nextX nextY) dirs where
    offset "n" = (0, 1)
    offset "s" = (0, -1)
    offset "ne" = (1, 0.5)
    offset "se" = (1, -0.5)
    offset "nw" = (-1, 0.5)
    offset "sw" = (-1, -0.5)
    offsetTuple = offset dir
    (nextX, nextY) = ((fst offsetTuple + x), (snd offsetTuple + y))
