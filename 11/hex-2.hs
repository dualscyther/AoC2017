import Data.List.Split

data Coord x y = Coord {getX :: x, getY :: y} deriving Show

main = do
    line <- getLine
    (putStrLn . show . ceiling . (\(_, steps) -> steps) . maxSteps . splitOn ",") line where
        nextMax (coord, steps) dir = let
            next = nextPos coord dir
            in (next, max steps (stepsNeeded next))
        maxSteps = foldl nextMax ((Coord 0 0), -1)

nextPos (Coord x y) dir = let
    offset "n" = (0, 1)
    offset "s" = (0, -1)
    offset "ne" = (1, 0.5)
    offset "se" = (1, -0.5)
    offset "nw" = (-1, 0.5)
    offset "sw" = (-1, -0.5)
    offsetTuple = offset dir
    in Coord (fst offsetTuple + x) (snd offsetTuple + y)

stepsNeeded (Coord x y) = (abs x) + abs ((abs y) -  0.5 * (abs x))
